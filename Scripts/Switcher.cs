using UnityEngine;
using UnityEngine.UI;


class Switcher
{
    Camera OtherCamera { get; set; }
    CameraControl OtherScript { get; set; }
    Image OtherCursor { get; set; }
    RaycastHit RayHit;
    GameObject MainCharacter;
    Camera MainCharacterCamera;
    CameraControl MainCharacterScript;
    Image MaincharacterCursor;
    public Camera CurrentCamera { get; set; }
    public CameraControl CurrentScript { get; set; }
    public Image CurrentCursor { get; set; }
    public Image progressCursor;
  

    public Switcher(RaycastHit rayHit)
    {
        RayHit = rayHit;

    }

    public Switcher()
    {

    }

    public void BasicSwitch()
    {
        if (DefineObjectsByHit())
        {
            Activator();
        }
    }

    public void MainCharacterSwitch()
    {
        GetMainCharachterData();
        ChangeTargetDataToMain();
        Activator();
    }

    Camera GetCamByHit()
    {
        return RayHit.transform.gameObject.GetComponent<Camera>();
    }

    CameraControl GetScriptByHit()
    {
        return RayHit.transform.gameObject.GetComponent<CameraControl>();
    }

    Image GetOuterCircle()
    {
        return RayHit.transform.gameObject.GetComponentInChildren<Image>();
    }

    bool DefineObjectsByHit()
    {
        this.OtherScript = GetScriptByHit();
        this.OtherCamera = GetCamByHit();
        this.OtherCursor = GetOuterCircle();
        return (OtherCamera != null && OtherScript != null);
    }

    void Activator()
    {
        SwitchCursors();
        SwitchCameras();
        SwitchScripts();
        ResetCursorState();
       
    }

    void SwitchCameras()
    {
        CurrentCamera.enabled = false;
        OtherCamera.enabled = true;
    }

    void SwitchScripts()
    {
        CurrentScript.enabled = false;
        OtherScript.enabled = true;
    }

    void SwitchCursors()
    {
       CurrentCursor.GetComponent<Image>().enabled = false;
       OtherCursor.GetComponent<Image>().enabled = true;
    }

    void ResetCursorState()
    {
        progressCursor.fillAmount = 0.3f;
    }

    void GetMainCharachterData()
    {
        MainCharacter = GameObject.FindWithTag("Main");
        MainCharacterCamera = MainCharacter.GetComponentInChildren<Camera>();
        MainCharacterScript = MainCharacter.GetComponentInChildren<CameraControl>();
        MaincharacterCursor = MainCharacter.GetComponentInChildren<Image>();
    }

    void ChangeTargetDataToMain()
    {
        this.OtherScript = MainCharacterScript;
        this.OtherCamera = MainCharacterCamera;
        this.OtherCursor = MaincharacterCursor;
    }







}


