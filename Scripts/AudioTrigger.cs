using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    AudioSource StageMusic;
    AudioSource Appleause;

    public Animator SingerAnimator;

    void Start()
    {
        StageMusic = this.transform.GetChild(0).GetComponent<AudioSource>();
        Appleause = this.transform.GetChild(2).GetComponent<AudioSource>();


        StartCoroutine(RandomWait());
    }

    void Update()
    {
        //Debug.Log(StageMusic.time);
        SingerController();
    }

    void StartSing()
    {

        SingerAnimator.SetBool("wait", false);

    }

    void StopSing()
    {

        SingerAnimator.SetBool("wait", true);

    }


    void SingerController()
    {
        if (StageMusic.time > 11f && StageMusic.time < 13f)
        {
            //Debug.Log("StartSing");
            StartSing();
        }
        else if (StageMusic.time > 117f && StageMusic.time < 120f)
        {
            StopSing();
            //Debug.Log("Solo");
        }
        else if (StageMusic.time > 155f && StageMusic.time < 158f)
        {
            StartSing();
            //Debug.Log("Resume Singing");
        }
    }


    IEnumerator RandomWait()
    {
        while (true)
        {
            Appleause.volume = Random.Range(0.2f, 1f);
            yield return new WaitForSeconds(Random.Range(5, 10));
        }
    }
}
