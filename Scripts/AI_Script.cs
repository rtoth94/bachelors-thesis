using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Linq;
using UnityEditor;

public class AI_Script : MonoBehaviour
{
    Animator animator;
    Dictionary<string, List<string>> graph =
            new Dictionary<string, List<string>>() {
        {"Idle", new List<string>{"to_State1","to_State2"}},
        {"State 1", new List<string>{"BacktoIdle","State1toState2"}},
        {"State 2", new List<string>{"BacktoIdle","State2toState1"}},
    };
    AnimationEvent evt;
    AnimatorClipInfo[] m_AnimatorClipInfo;
    AnimationClip cClip;
    public int loop_chance = 70;

    void Start()
    {
        animator = this.GetComponent<Animator>();

        foreach (AnimationClip ac in AClips())
        {
            evt = new AnimationEvent();
            evt.time = ac.length;
            evt.functionName = "Change";
            ac.AddEvent(evt);
        }
    }

    List<string> GetThePossibleTransitionsOfCurrentState()
    {
        foreach (string x in graph.Keys)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName(x))
            {
                return graph[x];
            }
        }
        return new List<string>();
    }

    public void Change()
    {
        List<string> poss = GetThePossibleTransitionsOfCurrentState();
        string choosen = poss[Random.Range(0, poss.Count)];
        float chance = 1 - (loop_chance / 100.0f);
        if (Random.value < chance)
        {
            animator.SetTrigger(choosen);
        }
    }

    AnimationClip CurrentClip()
    {
        m_AnimatorClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        return m_AnimatorClipInfo[0].clip;
    }

    AnimationClip[] AClips()
    {
        return animator.runtimeAnimatorController.animationClips;
    }

    //Last	
}
