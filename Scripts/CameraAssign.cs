using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraAssign : MonoBehaviour {

    public GameObject Head = null;

	// Use this for initialization
	void Start () {
        Canvas c = this.GetComponent<Canvas>();
        Camera cam = Head.GetComponent<Camera>();

        cam.tag = "MainCamera";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
