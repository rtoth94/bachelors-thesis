using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Starter : MonoBehaviour {

    public GameObject bob;
    Camera StartCam;

    

	// Use this for initialization
	void Start () {
        StartCam = gameObject.GetComponent<Camera>();
    }



    public void StartControl()
    {
        MovementScript ms = bob.GetComponent<MovementScript>();
        ms.enabled = true;

        Camera bc = bob.GetComponentInChildren<Camera>();
        bc.enabled = true;

        CameraControl cc = bob.GetComponentInChildren<CameraControl>();
        cc.enabled = true;

        StartCam.enabled = false;




    }
}
