using UnityEngine;
using UnityEngine.AI;

public class MovementScript : MonoBehaviour
{

    public Transform ObjectToSit;
    public float speed = 3F;

    Vector3 goal;
    Animator anim;
    NavMeshAgent agent;
    bool isSitting = false;
    Vector3 moveDirection = Vector3.zero;
    CharacterController controller;
    public DroneControler dcontroller;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        controller = GetComponent<CharacterController>();
        dcontroller = dcontroller.GetComponent<DroneControler>();
    }

    void Update()
    {

        if (isMainLeft())
        {
            GoToBar();
        }
        else if (isMainSitting())
        {
            StandUp();
        }
        else if (isMainControlled())
        {
            Stop();
        }
        ReachedGoal();
    }

    void FixedUpdate()
    {
        MovementController();
    }

    bool isAgentStopped()
    {
        return agent.isStopped;
    }

    bool isMainLeft()
    {
        return (!isMain() && !isSitting);
    }

    bool isMainSitting()
    {
        return (isMain() && isSitting);
    }

    bool isMainControlled()
    {
        return isMain() && !isSitting && !isAgentStopped();
    }

    public void StandUp()
    {
        Debug.Log("StandUp");
        agent.isStopped = true;
        anim.SetBool("move", false);
        anim.SetBool("stand", true);
        anim.SetBool("sit", false);
        isSitting = false;
        agent.updatePosition = true;
        controller.Move(Vector3.forward * 0.01f);
    }

    void Stop()
    {
        agent.isStopped = true;
        agent.updatePosition = true;
        anim.SetBool("stand", false);
    }

    void GoToBar()
    {
        GoToChair();
    }

    void MoveTowardsTarget(Vector3 target)
    {
        agent.updateRotation = true;
        agent.isStopped = false;
        agent.SetDestination(target);
        anim.SetBool("move", true);
        anim.SetBool("stand", false);

    }

    void OnTriggerStay(Collider other)
    {


        if (other.gameObject.tag == "Chair" && isAgentStopped() && !isSitting)
        {
            SitDown();
        }
    }


    void SitDown()
    {
        Debug.Log("Sit");
        isSitting = true;
        Quaternion targetRotation = Quaternion.LookRotation(ObjectToSit.forward);
        agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, targetRotation, Time.time * 1f);
        anim.SetBool("sit", true);
    }

    void ReachedGoal()
    {
        float distance = Vector3.Distance(transform.position, goal);
        if (distance < 0.25f && !isMain() && !isAgentStopped())
        {
            agent.isStopped = true;
            anim.SetBool("move", false);
        }
    }

    private void GoToChair()
    {
        if (!isSitting)
        {
            goal = ObjectToSit.transform.position + ObjectToSit.forward * 0.15f;
            Debug.DrawRay(goal, Vector3.up, Color.green);
            MoveTowardsTarget(goal);
        }
    }

    public void MovementController()
    {

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(x, 0, y);
        moveDirection = transform.TransformDirection(moveDirection);

        if (moveDirection.x != 0)
        {
            anim.SetBool("move", true);
        }
        else
        {
            anim.SetBool("move", false);
        }
        controller.Move(moveDirection * Time.deltaTime * speed);
    }

    public void Change()
    {
        //Debug.Log("Necessery");
    }

    bool isMain()
    {
        GameObject main = GameObject.FindWithTag("Main");
        CameraControl cc = main.GetComponentInChildren<CameraControl>();
        return cc.isActiveAndEnabled;
    }



    public void CallEnded()
    {
        Debug.Log("UseDrone");
        dcontroller.ExternalTrigger();
    }
    //Final 
}
