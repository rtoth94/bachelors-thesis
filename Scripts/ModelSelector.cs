using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelSelector : MonoBehaviour {

    public GameObject original;
    public GameObject custom;
   

	// Use this for initialization
	void Start () {
        original.SetActive(true);
        custom.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            SwitchCharacter();
        }
	}

    void SwitchCharacter()
    {
        GetInActiveObject().SetActive(true);
    }


    GameObject GetInActiveObject()
    {
        if (original.activeInHierarchy)
        {
            original.SetActive(false);
            return custom;
        }
        else
        {
            custom.SetActive(false);
            return original;
        }
    }


}
