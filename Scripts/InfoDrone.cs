using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoDrone : MonoBehaviour {

    public GameObject torus;
    public GameObject Canvas;
    public Image Help;
    public Image Icon;

	void Start () {
        Help = Help.GetComponent<Image>();
        Icon = Icon.GetComponent<Image>();
    }

	void Update () {
        torus.transform.Rotate(Vector3.up * 20 * Time.deltaTime,Space.World);


        if (Input.GetKeyDown(KeyCode.F1))
        {
            OpenPanel(!isActive());
        }
    }


    void OpenPanel(bool value)
    {
        Canvas.SetActive(value);
    }


    bool isActive()
    {
        return Canvas.activeInHierarchy;
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Main")
        {
            Help.enabled = true;
        }
    }



    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Main")
        {
            Help.enabled = false;
        }
    }






}
