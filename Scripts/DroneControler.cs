using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DroneControler : MonoBehaviour
{
    public Camera droneCamera;
    public GameObject MainCharacter;

    Rigidbody drone;
    MovementScript CharachterMovement;
    Vector3 limiter;
    AudioSource audio;
    AudioListener MainCharachterListener;
    AudioListener DroneListener;
    NavMeshAgent agent;
    CameraControl MainCharacterScript;
    public GameObject controller;


    float UpForce;
    bool starter = false;

    float ReferenceForwardVelocity;
    float ReferenceSideVelocity;
    float ReferenceRotationVelocity;

    float currentRotation;
    float targetRotation;

    float rotationAmount = 2f;
    float tiltAmount = 5f;
    float movementSpeed = 0.5f;

    float sideTiltAngle = 20f;
    float forwardTiltAngle = 20;

    float newSideTilt;
    float newForwardTilt;

    float treshold = 0.2f;


    void Start()
    {
        drone = this.GetComponent<Rigidbody>();
        CharachterMovement = MainCharacter.GetComponent<MovementScript>();
        MainCharachterListener = MainCharacter.GetComponentInChildren<AudioListener>();
        DroneListener = drone.GetComponentInChildren<AudioListener>();
        audio = drone.GetComponentInChildren<AudioSource>();
        MainCharacterScript = MainCharacter.GetComponentInChildren<CameraControl>();

        drone.useGravity = false;
        DroneListener.enabled = false;

    }

    void FixedUpdate()
    {
        if (starter)
        {
            FlyAndVerticalMovement();

            MoveAndTiltSide();
            MoveAndTiltForward();
            Rotate();

            SpeedLimit();
            ChangeDroneAttributes();

        }

    }

    void ChangeDroneAttributes()
    {
        drone.AddRelativeForce(Vector3.up * UpForce);
        drone.rotation = Quaternion.Euler(new Vector3(newForwardTilt, currentRotation, newSideTilt));
    }

    void FlyAndVerticalMovement()
    {


        bool Up = (Input.GetKeyDown(KeyCode.Keypad8) || (Input.GetAxis("Mouse Y") > 0));
        bool Down = (Input.GetKeyDown(KeyCode.Keypad8) || (Input.GetAxis("Mouse Y") < 0));

        if (Up && starter)
        {
            UpForce = 130;
        }
        else if (Down && starter)
        {
            UpForce = -30;
        }
        else if (!Up && !Down)
        {
            UpForce = 98.1f;
        }
    }

    void AudioController()
    {
        audio.pitch = 1 + ((drone.velocity.magnitude * 2) / 10);
        audio.volume = 0.004f + (drone.velocity.magnitude / 1000);
    }


    void MoveAndTiltForward()
    {
        float y = Input.GetAxis("Vertical");
        if (y != 0)
        {
            drone.AddRelativeForce(Vector3.forward * y * movementSpeed);
            newForwardTilt = TiltToDirection(y, newForwardTilt, forwardTiltAngle, ReferenceForwardVelocity);
        }
        else
        {
            newForwardTilt = ChangeTiltToZero(newForwardTilt, ReferenceForwardVelocity);
        }
    }

    void MoveAndTiltSide()
    {
        float x = Input.GetAxis("Horizontal");
        if (Mathf.Abs(x) > 0.2f)
        {
            drone.AddRelativeForce(Vector3.right * x * tiltAmount);
            newSideTilt = TiltToDirection(x, newSideTilt, -sideTiltAngle, ReferenceSideVelocity);
        }
        else
        {
            newSideTilt = ChangeTiltToZero(newSideTilt, ReferenceSideVelocity);
        }
    }

    float TiltToDirection(float Direction, float CurrentTiltValue, float TiltAngle, float Reference)
    {
        return Mathf.SmoothDamp(CurrentTiltValue, TiltAngle * Direction, ref Reference, 0.1f);
    }

    float ChangeTiltToZero(float CurrentTiltValue, float Reference)
    {
        return Mathf.SmoothDamp(CurrentTiltValue, 0, ref Reference, 0.1f);
    }




    void Rotate()
    {
        bool left = Input.GetKey(KeyCode.Keypad4) || (Input.GetAxis("Mouse X") < 0);
        bool right = Input.GetKey(KeyCode.Keypad6) || (Input.GetAxis("Mouse X") > 0);

        if (left)
        {
            targetRotation -= rotationAmount;
        }
        else if (right)
        {
            targetRotation += rotationAmount;
        }
        currentRotation = Mathf.SmoothDamp(currentRotation, targetRotation, ref ReferenceRotationVelocity, 0.2f);

    }

    void SpeedLimit()
    {

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");


        if (Mathf.Abs(x) > treshold && Mathf.Abs(y) > treshold)
        {
            Limiter();
        }
        if (Mathf.Abs(x) < treshold && Mathf.Abs(y) > treshold)
        {
            Limiter();
        }
        if (Mathf.Abs(x) > treshold && Mathf.Abs(y) < treshold)
        {
            Limiter();
        }
        if (Mathf.Abs(x) < treshold && Mathf.Abs(y) < treshold)
        {
            Stop();
        }
    }

    void Limiter()
    {
        drone.velocity = Vector3.ClampMagnitude(drone.velocity, Mathf.Lerp(drone.velocity.magnitude, 1.2f, Time.deltaTime * 45f));
    }

    void Stop()
    {
        drone.velocity = Vector3.SmoothDamp(drone.velocity, Vector3.zero, ref limiter, 0.2f);
    }

    void DisablePlayer()
    {
        CharachterMovement.enabled = false;
    }

    void EnablePlayer()
    {
        CharachterMovement.enabled = true;
    }

    void EnableFreeCam()
    {
        starter = true;
        droneCamera.enabled = true;
        drone.constraints = RigidbodyConstraints.None;
        DroneListener.enabled = true;
        MainCharachterListener.enabled = false;
        drone.useGravity = true;
        MainCharacter.GetComponentInChildren<Camera>().enabled = false;
        DisablePlayer();
    }

    void DisableFreeCam()
    {

        starter = false;
        drone.useGravity = false;
        drone.freezeRotation = true;
        droneCamera.enabled = false;
        DroneListener.enabled = false;
        MainCharachterListener.enabled = true; ;
        drone.constraints = RigidbodyConstraints.FreezeAll;
        MainCharacter.GetComponentInChildren<Camera>().enabled = true;
        EnablePlayer();

        if (MainCharacterScript.enabled != true)
        {
            MainCharacterScript.enabled = true;
        }
    }

    


   

    void Update()
    {
        Debug.Log(isMain());

        if ((Input.GetKeyDown("c") || Input.GetButtonDown("DS4_Triangle")) && !FreeCamStatus() && isMain())
        {
            MainCharacterScript.enabled = false;
            DisablePlayer();
            TriggerAnimation();

        }
        else if ((Input.GetKeyDown("c") || Input.GetButtonDown("DS4_Triangle")) && FreeCamStatus() && !isMain())
        {

            DisableFreeCam();
            controller.SetActive(false);
        }

        if (starter)
        {
            AudioController();
        }

       
    }


    bool isMain()
    {
        return MainCharacterScript.isActiveAndEnabled;
    }

    bool FreeCamStatus()
    {
        return starter;
    }

 

    void TriggerAnimation() {
        Animator anim = MainCharacter.GetComponent<Animator>();
        anim.SetTrigger("Drone");
        controller.SetActive(true);
    }



    public void ExternalTrigger()
    {
        EnableFreeCam();
    }




}
