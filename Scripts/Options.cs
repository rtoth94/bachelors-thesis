using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public GameObject OptionsPanel;
    public Slider SensitivitySlider;
    public Slider SpeedSlider;
    public Slider FOVSlider;
    public Slider LoadingTimeSlider;
    public GameObject MainCharacter;
    public Image LoadingCursor;

    static bool isLocked;
    Image ProgressCursor;
    Animator MainCharacterAnimatorComponent;

    void Start()
    {
        PanelVisible(false);

        ProgressCursor = LoadingCursor.GetComponentsInChildren<Image>(true)[1];
        MainCharacterAnimatorComponent = MainCharacter.GetComponent<Animator>();
    }

    public void ShowPanel()
    {
        if (ProgressCursor.fillAmount == 1)
        {
            PanelVisible(true);
            LockCamera();
        }
    }

    public static bool GetLockedState()
    {
        return isLocked;
    }

    void LockCamera()
    {
        isLocked = true;
        Cursor.visible = true;
        MainCharacterAnimatorComponent.enabled = false;
        LoadingCursor.gameObject.SetActive(false);
    }

    void ReleaseCam()
    {
        isLocked = false;
        Cursor.visible = false;
        MainCharacterAnimatorComponent.enabled = true;
        LoadingCursor.gameObject.SetActive(true);
    }

    void PanelVisible(bool visibility)
    {
        OptionsPanel.SetActive(visibility);
    }


    public void AppyButtonOnClick()
    {
        ReleaseCam();
        PanelVisible(false);
        CollectSliderValues();
    }


    void CollectSliderValues()
    {
        CameraControl CamControlReference = MainCharacter.GetComponentInChildren<CameraControl>();
        MovementScript MovementControlReference = MainCharacter.GetComponent<MovementScript>();

        
        CamControlReference.FOV = FOVSlider.value;
        CamControlReference.sensitivity = SensitivitySlider.value;
        CamControlReference.LoadingTime = LoadingTimeSlider.value;
        MovementControlReference.speed = SpeedSlider.value;

    }



}
