using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    public GameObject pauseMenu;
    private bool paused;
    public CameraControl MainCharacterScript;

    void Start()
    {
        pauseMenu.SetActive(false);
        paused = pauseMenu.activeInHierarchy;

    }

    void FixedUpdate()
    {
        if ((Input.GetKey(KeyCode.Escape) || Input.GetButtonDown("DS4_PAD")) && isMain())
        {
            Pause();
        }
    }

    bool isMain()
    {
        return MainCharacterScript.isActiveAndEnabled;
    }


    void Update()
    {
        paused = pauseMenu.activeInHierarchy;

        if (Input.GetButtonDown("DS4_X") && paused)
        {
            ResumeButtonOnClick();
        }
        if (Input.GetButtonDown("DS4_Circle") && paused)
        {
            ExitButtonOnClick();
        }
    }

    void Pause()
    {
        if (!paused)
        {
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
            Cursor.visible = true;
        }
        else if (paused)
        {
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
            Cursor.visible = false;

        }
    }

    public void ExitButtonOnClick()
    {
        Application.Quit();
    }

    public void ResumeButtonOnClick()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        Cursor.visible = false;

    }



}
