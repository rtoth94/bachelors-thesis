using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(AudioListener))]
public class CameraControl : MonoBehaviour
{

    public float sensitivity = 3F;
    public float LoadingTime = 1.5f;
    public float FOV = 60;


    public GameObject character = null;
    public float VerticalRotationLimit = 55f;
    public Image loading;

    float rotationY;
    float RawRotationX;
    float rotationX;
    float offset;

    Camera ThisCamera;
    Image progress;
    Image outerCircle;
    Vector3 HeadOriginalRotation;
    public bool camera_movement_lock = false;



    Options options;
    AudioListener listener;

  

    void Start()
    {
        Cursor.visible = false;

        progress = loading.GetComponentsInChildren<Image>(true)[1];
        outerCircle = loading.GetComponent<Image>();

        LoadIconVisibility(false);
        outerCircle.enabled = true;
        

        offset = this.transform.eulerAngles.y;
        HeadOriginalRotation = this.transform.eulerAngles;
        ThisCamera = this.GetComponent<Camera>();
        options = this.GetComponent<Options>();

        listener = this.GetComponent<AudioListener>();
     


    }

    void Update()
    {
        RayDetector();
        if (Input.GetKeyDown("q") || Input.GetButtonDown("DS4_Circle"))
        {

            if (!isMain())
            {
                JumpBackToMainCharacter();
            }
        }

        UpdateValuesAfterOptions();
        ListenerFollower();


    }

    void ListenerFollower()
    {
        listener.enabled = ThisCamera.isActiveAndEnabled;
    }


    void Rotator()
    {

        RawRotationX += Input.GetAxis("Mouse X") * sensitivity;
        rotationX = RawRotationX + offset;
        rotationY += Input.GetAxis("Mouse Y") * sensitivity;
        rotationY = Mathf.Clamp(rotationY, -VerticalRotationLimit, VerticalRotationLimit);

        if (!camera_movement_lock)
        {
            this.transform.eulerAngles = new Vector3(-rotationY, rotationX, 0);
        }

    }

    void LateUpdate()
    {
        Rotator();
        ForceBodyToFollowHead();
    }

    void LoadIconVisibility(bool enabled)
    {
        progress.enabled = enabled;
    }

    void CursorProgressStarter(bool start)
    {
        if (start)
        {
            LoadIconVisibility(true);
            progress.fillAmount += 1.0f / LoadingTime * Time.deltaTime;
        }
        else
        {
            LoadIconVisibility(false);
            progress.fillAmount = 0.3f;
        }
    }

    void RayDetector()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit))
        {
            if (hit.transform.tag == "Player")
            {
                CursorProgressStarter(true);
                Switch(hit);
            }
            else if (hit.transform.tag == "Options" && !camera_movement_lock && isMain())
            {
                CursorProgressStarter(true);
                options.ShowPanel();
            }
            else
            {
                CursorProgressStarter(false);
            }
        }
    }

    void Switch(RaycastHit hit)
    {
        if (progress.fillAmount == 1f)
        {

            Switcher sw = new Switcher(hit);
            sw.CurrentCamera = this.GetComponent<Camera>();
            sw.CurrentScript = this.GetComponent<CameraControl>();
            sw.progressCursor = progress;
            sw.CurrentCursor = loading;
            sw.BasicSwitch();
            progress.enabled = false;

        }
    }

    void ForceBodyToFollowHead()
    {
        if (isMain() && character != null)
        {
            Quaternion head = this.transform.rotation;
            Quaternion bdy = character.transform.rotation;
            character.transform.eulerAngles = new Vector3(bdy.eulerAngles.x, head.eulerAngles.y, bdy.eulerAngles.z);
        }
    }

    void CameraSetup()
    {
        ThisCamera.fieldOfView = FOV;
        ThisCamera.nearClipPlane = 0.2f;
        ThisCamera.farClipPlane = 100;
        ThisCamera.allowHDR = false;
        ThisCamera.allowMSAA = true;
    }

    bool isMain()
    {
        return (character != null && character.tag == "Main");
    }

    void JumpBackToMainCharacter()
    {
        Switcher sw2 = new Switcher();
        sw2.CurrentCamera = this.GetComponent<Camera>();
        sw2.CurrentScript = this.GetComponent<CameraControl>();
        sw2.progressCursor = progress;
        sw2.CurrentCursor = loading;
        sw2.MainCharacterSwitch();
        progress.enabled = false;

    }

    void UpdateValuesAfterOptions()
    {
        camera_movement_lock = Options.GetLockedState();
        ThisCamera.fieldOfView = FOV;
    }





    //Final Bracket
}
